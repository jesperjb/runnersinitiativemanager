import { Injectable } from '@angular/core';
import { Platform } from 'ionic-angular';
import { SQLite } from 'ionic-native';
import { Http } from '@angular/http';
import 'rxjs/add/operator/map';

const win: any = window;

/*
  Generated class for the Database provider.

  See https://angular.io/docs/ts/latest/guide/dependency-injection.html
  for more info on providers and Angular 2 DI.
*/
@Injectable()
export class Database {

  private storage: SQLite;

  public isOpen: Boolean = false;

  constructor(public platform: Platform, public http: Http) {
    this.platform.ready().then(() => {
        this.setUpDatabase();
    });
  }

  public setUpDatabase() {
      if (win.sqlitePlugin) {
          this.storage = win.sqlitePlugin.openDatabase({name: "data.db", location: "default"});
          this.isOpen = true;
      } else {
          console.warn('SQLite plugin not installed, falling back to WebSQL.');
          this.storage = win.openDatabase("data.db", '1.0', 'database', 5 * 1024 * 1024);
          this.isOpen = true;
      }
      this._tryInit();
  }

    /** Initialize the DB with our required tables */
    _tryInit() {
        this.query('CREATE TABLE IF NOT EXISTS encounters (id INTEGER PRIMARY KEY AUTOINCREMENT, title TEXT, description TEXT)')
        .catch(err => {
            console.error('Unable to create initial storage tables', err.tx, err.err);
        });
    }

    query(query: string, params: any[] = []): Promise<any> {
        return new Promise((resolve, reject) => {
            try {
                console.log(query);
                console.log(this.storage);
                this.storage.transaction((tx: any) => {
                    tx.executeSql(
                        query,
                        params,
                        (tx: any, res: any) => resolve({ tx: tx, res: res }),
                        (tx: any, err: any) => reject({ tx: tx, err: err })
                    );
                });
            } catch (err) {
                reject({ err: err });
            }
        });
    }

  public getEncounters() {
    return new Promise((resolve, reject) => {
      this.query("SELECT * FROM encounters", []).then((data) => {
        let encounters = [],
            rows = data.res.rows;
        if(rows.length > 0) {
          for(let i = 0; i < rows.length; i++) {
            encounters.push({
              id: rows.item(i).id,
              title: rows.item(i).title,
              description: rows.item(i).description
            });
          }
        }
        resolve(encounters);
      }, (error) => {
        reject(error);
      });
    });
  }

  public createEncounter(title: string, description: string) {
    return new Promise((resolve, reject) => {
      this.query("INSERT INTO encounters (title, description) VALUES (?, ?)", [title, description]).then((data) => {
        resolve(data);
      }, (error) => {
        reject(error);
      });
    });
  }

}
