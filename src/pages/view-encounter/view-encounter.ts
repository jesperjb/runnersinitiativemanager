import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';

/*
  Generated class for the ViewEncounter page.

  See http://ionicframework.com/docs/v2/components/#navigation for more info on
  Ionic pages and navigation.
*/
@Component({
  selector: 'page-view-encounter',
  templateUrl: 'view-encounter.html'
})
export class ViewEncounterPage {

  public encounter: any;

  constructor(public navCtrl: NavController, public params:NavParams) {
    this.encounter = params.get('encounter');
  }

  ionViewDidLoad() {
    console.log('Hello ViewEncounterPage Page');
  }

}
