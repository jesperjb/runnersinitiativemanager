import { Component } from '@angular/core';

import { EncounterListPage } from '../encounter-list/encounter-list';
import { ParticipantListPage } from '../participant-list/participant-list';
import { ItemListPage } from '../item-list/item-list';
import { ContactPage } from '../contact/contact';

@Component({
  templateUrl: 'tabs.html'
})
export class TabsPage {
  tab1Root: any = EncounterListPage;
  tab2Root: any = ParticipantListPage;
  tab3Root: any = ItemListPage;
  tab4Root: any = ContactPage;

  constructor() {

  }
}
