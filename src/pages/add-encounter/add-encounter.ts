import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';

/*
  Generated class for the AddEncounter page.

  See http://ionicframework.com/docs/v2/components/#navigation for more info on
  Ionic pages and navigation.
*/
@Component({
  selector: 'page-add-encounter',
  templateUrl: 'add-encounter.html'
})
export class AddEncounterPage {

  constructor(public navCtrl: NavController) {}

  ionViewDidLoad() {
    console.log('Hello AddEncounterPage Page');
  }

}
