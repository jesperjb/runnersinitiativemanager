import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';

/*
  Generated class for the Items list page.

  See http://ionicframework.com/docs/v2/components/#navigation for more info on
  Ionic pages and navigation.
*/
@Component({
  selector: 'page-item-list',
  templateUrl: 'item-list.html'
})
export class ItemListPage {

  items = [];
  groupeditems = [];

  constructor(public navCtrl: NavController) {
    this.items = [
      'Kate Beckett',
      'Richard Castle',
      'Alexis Castle',
    ];

    this.groupItems(this.items);
  }

  ionViewDidLoad() {
    console.log('Hello Item list Page');
  }

  groupItems(items){

    let sortedItems = items.sort();
    let currentLetter = false;
    let currentItems = [];

    sortedItems.forEach((value, index) => {

      if(value.charAt(0) != currentLetter){

        currentLetter = value.charAt(0);

        let newGroup = {
          letter: currentLetter,
          contacts: []
        };

        currentItems = newGroup.contacts;
        this.groupeditems.push(newGroup);

      }

      currentItems.push(value);

    });

  }

}
