import { Component } from '@angular/core';
import { Platform, NavController, AlertController } from 'ionic-angular';
import { Database } from "../../providers/database";
import { ViewEncounterPage } from '../view-encounter/view-encounter';

/*
  Generated class for the EncounterList page.

  See http://ionicframework.com/docs/v2/components/#navigation for more info on
  Ionic pages and navigation.
*/
@Component({
  selector: 'page-encounter-list',
  templateUrl: 'encounter-list.html'
})
export class EncounterListPage {

  encounters: any = [];

  constructor(public platform: Platform, public navCtrl: NavController, private alertCtrl: AlertController, public database: Database) {
    this.encounters = [];
  }

  public onPageDidEnter() {
    this.platform.ready().then(() => {
      this.database.setUpDatabase();
      if (this.database.isOpen) {
        //this.load();
      } else {
        setTimeout(function() {
          this.load();
        }, 5000);
      }
    });
  }

  public load() {
    this.database.getEncounters().then((result) => {
      this.encounters = <Array<Object>> result;
    }, (error) => {
      console.log("ERROR: ", error);
    });
  }

  ionViewDidLoad() {
      this.load();
  }

  addEncounter() {
    let prompt = this.alertCtrl.create({
      title: 'Add Encounter',
      inputs: [{
        name: 'title',
        placeholder: 'title'
      },
      {
        name: 'description',
        placeholder: 'description'
      }],
      buttons: [
        {
          text: 'Cancel'
        },
        {
          text: 'Add',
          handler: data => {
            this.database.createEncounter(data.title, data.description).then((result) => {
              this.load();
            }, (error) => {
              console.log("ERROR: ", error);
            });
          }
        }
      ]
    });

    prompt.present();
  }

  editEncounter(enc) {
    let prompt = this.alertCtrl.create({
      title: 'Edit Encounter',
      inputs: [{
        name: 'title',
        placeholder: 'title',
        value: enc.title,
      },
      {
        name: 'description',
        placeholder: 'description',
        value: enc.description
      }],
      buttons: [
        {
          text: 'Cancel'
        },
        {
          text: 'Save',
          handler: data => {
            let index = this.encounters.indexOf(enc);

            if(index > -1){
              this.encounters[index] = data;
            }
          }
        }
      ]
    });

    prompt.present();
  }

  deleteEncounter(enc) {
    let index = this.encounters.indexOf(enc);

    if(index > -1){
      this.encounters.splice(index, 1);
    }
  }

  viewEncounter(enc) {
    this.navCtrl.push(ViewEncounterPage,{ encounter: enc.id});
  }

}
