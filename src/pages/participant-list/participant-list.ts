import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';

/*
  Generated class for the ParticipantList page.

  See http://ionicframework.com/docs/v2/components/#navigation for more info on
  Ionic pages and navigation.
*/
@Component({
  selector: 'page-participant-list',
  templateUrl: 'participant-list.html'
})
export class ParticipantListPage {

  participants = [];
  groupedParticipants = [];

  constructor(public navCtrl: NavController) {
    this.participants = [
      'Kate Beckett',
      'Richard Castle',
      'Alexis Castle',
      'Lanie Parish',
      'Javier Esposito',
      'Kevin Ryan',
      'Martha Rodgers',
      'Roy Montgomery',
      'Jim Beckett',
      'Stana Katic',
      'Nathan Fillion',
      'Molly Quinn',
      'Tamala Jones',
      'Jon Huertas',
      'Seamus Dever',
      'Susan Sullivan'
    ];

    this.groupParticipants(this.participants);
  }

  ionViewDidLoad() {
    console.log('Hello ParticipantListPage Page');
  }

  groupParticipants(participants){

    let sortedParticipants = participants.sort();
    let currentLetter = false;
    let currentParticipants = [];

    sortedParticipants.forEach((value, index) => {

      if(value.charAt(0) != currentLetter){

        currentLetter = value.charAt(0);

        let newGroup = {
          letter: currentLetter,
          contacts: []
        };

        currentParticipants = newGroup.contacts;
        this.groupedParticipants.push(newGroup);

      }

      currentParticipants.push(value);

    });

  }

}
